const express = require('express')
const router = express.Router()
const HTTPStatus = require('http-status')
const controller = require('modules/posts/controllers')
const errors = require('helpers/error/errors')
const { postValidator } = require('modules/posts/validations')
const logger = require('helpers/logger')

router.get('/', async (req, res, next) => {
  logger.info('get all posts')
  const posts = await controller.find({})
  res
    .status(HTTPStatus.OK)
    .json(posts.map(mapPostData))
})

router.post('/', postValidator, async (req, res, next) => {
  logger.info('creating a post')
  const post = await controller.save({ data: req.body })
  res
    .status(HTTPStatus.CREATED)
    .json({ id: post.id })
})

router.put('/:id', async (req, res, next) => {
  logger.info('updating a post')
  let data = {}
  data = req.body
  data.id = req.params.id
  await controller.update({ data })
  res
    .status(HTTPStatus.NO_CONTENT)
    .json({})
})

router.delete('/:id', async (req, res, next) => {
  logger.info('removing a post')
  try {
    let data = {}
  data.id = req.params.id
  await controller.delete({ data })
  res
    .status(HTTPStatus.NO_CONTENT)
    .json({})
  } catch (err) {
    if (err.code === errors.ERR_NOT_FOUND) {
      err.status = HTTPStatus.NOT_FOUND
    }
    return next(err)
  }
})

router.get('/:id', async (req, res, next) => {
  logger.info('get a post by id')
  try {
  let data = {}
  data.id = req.params.id
  const post = await controller.findById({ data })
  res
    .status(HTTPStatus.OK)
    .json(mapPostData(post))
  } catch (err) {
    if (err.code === errors.ERR_NOT_FOUND) {
      err.status = HTTPStatus.NOT_FOUND
    }
    return next(err)
  }
})

const mapPostData = item => ({
  id: item._id,
  title: item.title,
  content: item.content,
  author: item.author,
  created: item.createdAt,
  movieName: item.movieName,
  comments: item.comments.length ? item.comments.map(comment => ({
    author: comment.author,
    comment: comment.comment,
    createdAt: comment.createdAt
  })) : [],
  omdbInfo: item.omdbInfo
})

module.exports = router
