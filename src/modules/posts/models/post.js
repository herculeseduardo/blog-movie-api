const mongoose = require('mongoose')
const Schema = mongoose.Schema

const commentSchema = new Schema({
  author: {
    required: true,
    type: String
  },
  comment: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

const postSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    required: true,
    type: String
  },
  author: {
    required: true,
    type: String
  },
  movieName: {
    required: true,
    type: String
  },
  comments: [commentSchema],
  createdAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('Post', postSchema)
