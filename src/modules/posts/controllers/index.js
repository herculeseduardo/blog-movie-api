const Post = require('modules/posts/models/post')
const logger = require('helpers/logger')
const AppError = require('helpers/error')
const errors = require('helpers/error/errors')

const integration = require('integrations/omdb')

const controller = {}

controller.save = async ({ data }) => {
  const post = new Post(data).save()
  return post
}

controller.update = async ({ data }) => {
  await Post.updateOne({
    _id: data.id
  }, data, { upsert: true })

  logger.info('updated')
}

controller.find = async ({ data }) => {
  return await Post.find(data)
}

controller.delete = async ({ data }) => {
  await controller.findById({ data })
  await Post.deleteOne({
    _id: data.id
  })
}

controller.findById = async ({ data }) => {
  const [post] =  await Post.find({
    _id: data.id
  })

  if (!post) throw new AppError(errors.ERR_NOT_FOUND, 'Not Found')

  let title = post.movieName
  const response = await integration.getOmdbInfo({ title })

  if (response) {
    logger.info('Omdb found')
    const obj = post.toObject()
    obj.omdbInfo = response
    return obj
  }

  return post
}

module.exports = controller
