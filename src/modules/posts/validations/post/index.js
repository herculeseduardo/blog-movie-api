const joi = require('joi')
const validation = require('helpers/middleware/validation')

const schema = {
  title: joi.string().required(),
  content: joi.string().required(),
  author: joi.string().required(),
  movieName: joi.string().required(),
  comments: joi.array().items(joi.object().keys({
    comment: joi.string(),
    author: joi.string()
  }))
}

module.exports = {
  schema,
  validate: validation(schema)
}