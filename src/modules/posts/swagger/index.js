/**
 * @swagger
 * definitions:
 *   PostRequest:
 *     properties:
 *       title:
 *         type: string
 *       content:
 *         type: string
 *       author:
 *         type: string
 *       movieName:
 *         type: string
 *       comments:
 *         type: array
 *         items:
 *           type: object
 *           properties:
 *             author:
 *               type: string
 *             comment:
 *               type: string
 */

/**
 * @swagger
 * definitions:
 *   PostResponse:
 *     properties:
 *       id:
 *         type: number
 *       title:
 *         type: string
 *       content:
 *         type: string
 *       author:
 *         type: string
 *       movieName:
 *         type: string
 *       comments:
 *         type: array
 *         items:
 *           type: object
 *           properties:
 *             author:
 *               type: string
 *             comment:
 *               type: string
 *             createdAt:
 *               type: string
 *               format: date
 */

/**
 * @swagger
 * definitions:
 *   PostId:
 *     properties:
 *       id:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   Posts:
 *     type: array
 *     items:
 *       $ref: '#/definitions/PostResponse'
 */

/**
 * @swagger
 * /api/post:
 *   get:
 *     summary: get posts
 *     tags:
 *       - posts
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *         schema:
 *           $ref: '#/definitions/Posts'
 *       404:
 *         description: Not found
 */

 /**
 * @swagger
 * /api/post:
 *   post:
 *     summary: save post
 *     tags:
 *       - posts
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Post
 *         type: object
 *         schema:
 *           $ref: '#/definitions/PostRequest'
 *     responses:
 *       201:
 *         description: Created
 *         schema:
 *           $ref: '#/definitions/PostId'
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not found
 */

 /**
 * @swagger
 * /api/post/{postId}:
 *   put:
 *     summary: update post
 *     tags:
 *       - posts
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: postId
 *         description: postId
 *         required: true
 *       - in: body
 *         name: body
 *         description: Post
 *         type: object
 *         schema:
 *           $ref: '#/definitions/PostResponse'
 *     responses:
 *       204:
 *         description: No content
 *       404:
 *         description: Not found
 */

/**
 * @swagger
 * /api/post/{postId}:
 *   delete:
 *     summary: delete post
 *     tags:
 *       - posts
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: postId
 *         description: postId
 *         required: true
 *     responses:
 *       204:
 *         description: No content
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not found
 */

/**
 * @swagger
 * /api/post/{postId}:
 *   get:
 *     summary: get post by id
 *     tags:
 *       - posts
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: postId
 *         description: postId
 *         required: true
 *     responses:
 *       200:
 *         description: OK
 *         schema:
 *           $ref: '#/definitions/PostResponse'
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not found
 */
