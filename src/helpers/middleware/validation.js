const joi = require('joi')
const errors = require('helpers/error/errors')
const HTTPStatus = require('http-status')

module.exports = (schema) => {
  return (req, res, next) => {
    let err = joi.validate(req.body, schema, {
      abortEarly: false
    })

    if (err.error) {
      const { details } = err.error
      const message = details.map(i => i.message).join(',')

      err.error.status = HTTPStatus.BAD_REQUEST
      err.error.code = 'ERR_BAD_REQUEST' //errors.ERR_BAD_REQUEST
      err.error.message = message
      return next(err.error)
    } else {
      return next()
    }
  }
}