class AppError extends Error {
  constructor (code, message) {
    super(message)
    this.message = message
    this.name = 'BLOG_APPLICATION'
    this.code = code
  }
}

module.exports = AppError
