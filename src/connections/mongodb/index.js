const mongoose = require('mongoose')
const logger = require('helpers/logger')

let mongo = {}

mongo.getConnection = () => {
  logger.info(`Connecting on mongoDB mongodb://${process.env.MONGO_HOST}/${process.env.MONGO_DATABASE}`)

  mongoose.set('useUnifiedTopology', true)

  return mongoose.connect(`mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`, { useNewUrlParser: true })
}

module.exports = mongo
