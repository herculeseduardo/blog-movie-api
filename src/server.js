require('dotenv').config()
const express = require('express')
const http = require('http')
const logger = require('helpers/logger')
const errors = require('helpers/error/errors')
const HTTPStatus = require('http-status')

const mongo = require('connections/mongodb')
const app = express()

const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const port = process.env.PORT || 3000

mongo.getConnection().then(conn => {
  logger.info(`mongodb connected!`)
}).catch(console.error)

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(bodyParser.text())
app.use(cookieParser())

logger.stream = {
  write: function (message, encoding) {
    logger.debug(message)
  }
}
app.use(require('morgan')('tiny', { 'stream': logger.stream }))

// routes here
app.use('/api/post', require('modules/posts'))

const swagger = require('helpers/swagger')({ apis: [
    `./src/modules/posts/**/*.js`
] })

app.use('/swagger', swagger.router)
app.get('/', (req, res, next) => res.redirect('/swagger'))

app.use((req, res, next) => {
  let err = new Error(HTTPStatus[HTTPStatus.NOT_FOUND])
  err.status = HTTPStatus.NOT_FOUND
  err.code = errors.ERR_NOT_FOUND
  next(err)
})

app.use((err, req, res, next) => {
  res.status(err.status || HTTPStatus.INTERNAL_SERVER_ERROR)
  if (!err.code) {
    console.trace(err)
  }
  res.json(err)
})

const server = http.createServer(app)

server.listen(port)

server.on('listening', server => {
  logger.info(`Server ${process.env.HOSTNAME} listening on ${port}...`)
})
