const request = require('request-promise')
const logger = require('helpers/logger')
const integration = {}

integration.getOmdbInfo = async ({ title }) => {
  logger.info('Omdb integration')
  const options = {
    url: `http://www.omdbapi.com/?t=${title}&apikey=${process.env.OMDB_API_KEY}`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    json: true
  }
  return await request.get(options)
}

module.exports = integration
