FROM node:11-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .

RUN npm install

EXPOSE 3000

ENV MONGO_HOST=mongo

ENV MONGO_PORT=27017

ENV MONGO_DATABASE=blog

CMD ["npm", "start"]