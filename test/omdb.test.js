const integration = require('integrations/omdb')

describe('Integration test suite', () => {

  it('should fetch movie info', async () => {
    let title = 'Inception'
    const response = await integration.getOmdbInfo({ title })
    console.log(response)
  })
})
