require('dotenv').config()
const expect = require('chai').assert
const faker = require('faker')

const mongo = require('connections/mongodb')
const Post = require('modules/posts/models/post')
const controller = require('modules/posts/controllers')
const logger = require('helpers/logger')

describe('Posts test suite', () => {

  let postTitle

  before(async () => {
    await mongo.getConnection()

    await Post.deleteMany({})

    postTitle = faker.random.word()

    logger.info(postTitle)
  })

  it('should save a post', async () => {
    const name = faker.name.findName()
    const input = {
      title: postTitle,
      content: faker.lorem.sentence(),
      author: name,
      movieName: faker.name.jobArea(),
      comments: {
        author: name,
        comment: faker.lorem.paragraph()
      }
    }
    
    const response = await controller.save({ data: input })

    logger.info(response.title)
    
    expect(response.title, input.title)
  })

  it('should retrieve a post', async () => {
    const post = await controller.find({})
    expect(post.length, 1)
  })

  it('should update a post', async () => {
    const [post] = await controller.find({ title: postTitle })
    logger.info(post.title)

    expect(post.title, postTitle)
    
    const newTitle = faker.random.word()

    await controller.update({ data: { id: post.id, title: newTitle } })

    const [postUpdated] = await controller.find({ title: newTitle })
    logger.info(postUpdated.title)

    expect(postUpdated.title, newTitle)

    postTitle = newTitle
  })

  it('should remove a post', async () => {
    const [post] = await controller.find({ title: postTitle })
    logger.info(post.title)

    await controller.delete({ data: post })
  })
})
