## blog-movie-api

api para crud de post de um blog

# Passos para rodar a aplicação

1. Clonar repositório

* git clone https://gitlab.com/herculeseduardo/blog-movie-api.git

2. Entrar na pasta

* cd blog-movie-api/

3. Rodar docker compose ( 2 servers )

* docker-compose up --scale api=2 --build -d


# Operações

# OBS.:
O traefik fará o load balacing dos servers, portanto será necessário abrir
o arquivo /etc/hosts e adicionar o seguinte host: "blogapp.localhost"

Ex.:
127.0.0.1	localhost blogapp.localhost

1. Acessar o endereço abaixo para painel do traefik

http://localhost:8080

2. Na caixa services clicar no link "Explore"

3. Clicar no link "api_blog-movie-api@docker"

4. Verifique as URLs do serviço na caixa Servers

Ex. http://172.20.0.5:3000 e http://172.20.0.4:3000

5. Acessar a URL http://blogapp.localhost

6. A documentação swagger será exibida

* Obter todos os posts

| Verb     | endpoint          |
| -------- |:-----------------:|
| GET      | /api/post         |

Nenhum payload

* Criar um post

| Verb     | endpoint          |
| -------- |:-----------------:|
| POST     | /api/post         |

Exemplo de payload

```
{
  "title": "Filmes legais",
  "content": "Este filme é uma obra prima, ...",
  "author": "Mario Cardoso",
  "movieName": "Pulp Fiction",
  "comments": [
    {
      "author": "André Ribas",
      "comment": "Também gostei",
      "createdAt": "2019-10-08"
    }
  ]
}
```

* Atualizar um post pelo ID

| Verb     | endpoint          |
| -------- |:-----------------:|
| PUT      | /api/post/:postId |

Exemplo de payload para alterar apenas o conteúdo

```
{
  "content": "Este filme é uma obra prima, ... Em tempos de violência",
}
```

* Obter um post pelo ID

| Verb     | endpoint          |
| -------- |:-----------------:|
| GET      | /api/post/:postId |

Nenhum payload. Verificar o campo omdbInfo

* Deletar um post pelo ID

| Verb     | endpoint          |
| -------- |:-----------------:|
| DELETE   | /api/post/:postId |

Nenhum payload
